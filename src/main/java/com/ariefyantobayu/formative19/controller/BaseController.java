package com.ariefyantobayu.formative19.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class BaseController {
    public static Scanner scanner = new Scanner(System.in);

    @Autowired
    AuthorConsoleController authorConsoleController;

    @Autowired
    BookConsoleController bookConsoleController;

    public void run() {
        String menuSelection;
        do {
            System.out.println("====MAIN MENU====");
            System.out.println("1. Sub Menu Books\n2. Sub Menu Author");
            System.out.print("Choice : ");
            menuSelection = scanner.nextLine();

            if (menuSelection.equals("1")) {
                bookConsoleController.run(scanner);
            } else if (menuSelection.equals("2")) {
                authorConsoleController.run(scanner);
            } else {
                System.out.println("[Please, Select between 1 or 2]");
            }

        } while (true);
    }
}
