package com.ariefyantobayu.formative19.controller;


import com.ariefyantobayu.formative19.entity.Author;
import com.ariefyantobayu.formative19.repository.AuthorRepository;
import com.ariefyantobayu.formative19.view.AuthorView;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

@Component
public class AuthorConsoleController {
    private Scanner scanner;

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    AuthorView authorView;



    public AuthorConsoleController(AuthorView authorView) {
        this.authorView = authorView;
        System.out.println("Author Controller Initialized");
    }

    public void run(Scanner scanner) {
        this.scanner = scanner;
        String menuSelection;

        do {
            authorView.printMenu();
            menuSelection = scanner.nextLine();

            switch(menuSelection) {
                case "1":
                    getAllAuthor();
                    break;
                case "2":
                    getAuthorById();
                    break;
                case "3":
                    System.out.println("[NOT YET IMPLEMENTED]");
                    break;
                case "4":
                    System.out.println("[NOT YET IMPLEMENTED]");
                    break;
                default:
                    menuSelection = "0";
            }
        } while (!menuSelection.equalsIgnoreCase("0"));

    }

    public boolean getAllAuthor() {
        List<Author> authors = authorRepository.findAll();
        if (authors != null && authors.size() > 0) {
            authorView.printAll(authors);
            return true;
        } else {
            authorView.printBannerWithBody("DATA NOT FOUND", "Data not found or empty");
            return false;
        }
    }

    public boolean getAuthorById() {
        String id;
        long idInNumber;
        Author author;
        System.out.print("Insert Author ID : ");
        id = scanner.nextLine();

        idInNumber = Long.parseLong(id);
        author = authorRepository.findById(idInNumber);
        if (author != null) {
            authorView.printAuthor(author);
            return true;
        } else {
            authorView.printBannerWithBody("DATA NOT FOUND", "Data not found");
            return false;
        }

    }
}
