package com.ariefyantobayu.formative19.controller;

import com.ariefyantobayu.formative19.entity.Author;
import com.ariefyantobayu.formative19.entity.Book;
import com.ariefyantobayu.formative19.repository.AuthorRepository;
import com.ariefyantobayu.formative19.repository.BookRepository;
import com.ariefyantobayu.formative19.view.BookView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

@Component
public class BookConsoleController {
    private Scanner scanner;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BookView bookView;

    public BookConsoleController(BookView bookView) {
        this.bookView = bookView;
        System.out.println("Book Controller Initialized");
    }

    public void run(Scanner scanner) {
        this.scanner = scanner;
        String menuSelection;

        do {
            bookView.printMenu();
            menuSelection = scanner.nextLine();

            switch(menuSelection) {
                case "1":
                    getAllBook();
                    break;
                case "2":
                    getBookById();
                    break;
                case "3":
                    getAllByAuthorId();
                    break;
                case "4":
                    System.out.println("[NOT YET IMPLEMENTED]");
                    break;
                case "5":
                    System.out.println("[NOT YET IMPLEMENTED]");
                    break;
                default:
                    menuSelection = "0";
            }
        } while (!menuSelection.equalsIgnoreCase("0"));

    }

    public boolean getAllBook() {
        List<Book> books = bookRepository.findAll();
        if (books != null && books.size() > 0) {
            bookView.printAll(books);
            return true;
        } else {
            bookView.printBannerWithBody("DATA NOT FOUND", "Data not found or empty");
            return false;
        }
    }

    public boolean getAllByAuthorId() {
        long id;
        List<Book> books;
        System.out.print("Insert Author ID : ");
        id = Long.parseLong(scanner.nextLine());

        books = bookRepository.findyByAuthorId(id);

        if (books != null && books.size() > 0) {
            bookView.printAll(books);
            return true;
        } else {
            bookView.printBannerWithBody("DATA NOT FOUND", "Data not found");
            return false;
        }
    }

    public boolean getBookById() {
        String id;
        long idInNumber;
        Book book;
        System.out.print("Insert Book ID : ");
        id = scanner.nextLine();

        idInNumber = Long.parseLong(id);
        book = bookRepository.findById(idInNumber);
        if (book != null) {
            bookView.printBook(book);
            return true;
        } else {
            bookView.printBannerWithBody("DATA NOT FOUND", "Data not found");
            return false;
        }

    }
}
