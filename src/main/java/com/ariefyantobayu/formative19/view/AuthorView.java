package com.ariefyantobayu.formative19.view;

import com.ariefyantobayu.formative19.entity.Author;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthorView {

    public static String BANNER = "===============================\n";

    public void printAll(List<Author> authorList) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Author author : authorList) {
            stringBuilder.append(String.format("ID : %s\nName : %s\nEmail : %s\n",
                    author.getId(),
                    author.getName(),
                    author.getEmail()));

            stringBuilder.append(BANNER);
        }

        printBannerWithBody("ALL AUTHOR DATA", stringBuilder.toString());
    }

    public void printMenu() {
        System.out.println("1. View All Author\n2. View Author By Id\n3. Add Author [Not Implemented]\n4. Delete Author [Not Implemented]\n0. Back");
        System.out.print("Pilih : ");
    }

    public void printBanner(String title) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < Math.floor((double) (BANNER.length() - title.length()) / 2.0); i++) {
            stringBuilder.append("=");
        }
        stringBuilder.append(title);
        for (int i = 0; i < Math.ceil((double) (BANNER.length() - title.length()) / 2.0) - 1; i++) {
            stringBuilder.append("=");
        }

        stringBuilder.append('\n');
        System.out.print(stringBuilder.toString());
    }

    public void printBannerWithBody(String title, String body) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(body + '\n');
        stringBuilder.append(BANNER);
        printBanner(title);
        System.out.print(stringBuilder.toString());
    }

    public void printAuthor(Author author) {
        String body = String.format("ID : %d\nName : %s\nEmail : %s",
                author.getId(),
                author.getName(),
                author.getEmail());

        printBannerWithBody("DATA AUTHOR", body);
    }
}
