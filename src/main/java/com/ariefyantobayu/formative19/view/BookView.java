package com.ariefyantobayu.formative19.view;

import com.ariefyantobayu.formative19.entity.Author;
import com.ariefyantobayu.formative19.entity.Book;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookView {
    public static String BANNER = "===============================\n";

    public void printAll(List<Book> bookList) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Book book : bookList) {
            stringBuilder.append(String.format("ID : %d\nTitle : %s\nPublished : %s\nCity : %s\nAuthor : %s\n",
                    book.getId(),
                    book.getTitle(),
                    book.getPublishYear(),
                    book.getCity(),
                    book.getAuthor().getName()
            ));

            stringBuilder.append(BANNER);
        }

        printBannerWithBody("ALL BOOK DATA", stringBuilder.toString());
    }

    public void printMenu() {
        System.out.println("1. View All Book\n2. View Book By Id\n3. View Book By Author Id\n4. Add Author [Not Implemented]\n5. Delete Author [Not Implemented]\n0. Back");
        System.out.print("Pilih : ");
    }

    public void printBanner(String title) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < Math.floor((double) (BANNER.length() - title.length()) / 2.0); i++) {
            stringBuilder.append("=");
        }
        stringBuilder.append(title);
        for (int i = 0; i < Math.ceil((double) (BANNER.length() - title.length()) / 2.0) - 1; i++) {
            stringBuilder.append("=");
        }

        stringBuilder.append('\n');
        System.out.print(stringBuilder.toString());
    }

    public void printBannerWithBody(String title, String body) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(body + '\n');
        stringBuilder.append(BANNER);
        printBanner(title);
        System.out.print(stringBuilder.toString());
    }

    public void printBook(Book book) {
        String body = String.format("ID : %d\nTitle : %s\nPublished : %s\nCity : %s\nAuthor : %s\nContact : %s\n",
                book.getId(),
                book.getTitle(),
                book.getPublishYear(),
                book.getCity(),
                book.getAuthor().getName(),
                book.getAuthor().getEmail()
                );

        printBannerWithBody("DETAIL BOOK", body);
    }
}
