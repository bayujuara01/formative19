package com.ariefyantobayu.formative19.repository;

import com.ariefyantobayu.formative19.entity.Book;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class BookRepository {

    @PersistenceContext
    EntityManager entityManager;

    public List<Book> findAll() {
        TypedQuery<Book> query = entityManager.createQuery("SELECT b FROM Book b", Book.class);
        return query.getResultList();
    }

    public Book findById(long id) {
        Book book = null;
        TypedQuery<Book> query = entityManager.createQuery("SELECT b FROM Book b WHERE b.id=:id", Book.class);
        query.setParameter("id", id);

        try {
            book = query.getSingleResult();
        } catch (Exception e) {
            book = null;
        }
        return book;
    }

    public List<Book> findyByAuthorId(long id) {
        TypedQuery<Book> query = entityManager.createQuery("SELECT b FROM Book b WHERE b.author.id = :id", Book.class);
        query.setParameter("id", id);
        return query.getResultList();
    }
    
    public List<Book> findByAuthorName(String name) {
    	TypedQuery<Book> query = entityManager.createQuery("SELECT b FROM Book b WHERE b.author.name LIKE :name", Book.class);
    	query.setParameter("name", "%"+name+"%");
    	return query.getResultList();
    }


    public int insert(Book book) {
        Query query = entityManager.createNativeQuery("INSERT INTO book (title, publish_year, city, author_id) VALUES (:title, :publish, :city, :author)");
        query.setParameter("title", book.getTitle());
        query.setParameter("publish", book.getPublishYear());
        query.setParameter("city", book.getCity());
        query.setParameter("author", book.getAuthor().getId());
        return query.executeUpdate();
    }

    public int deleteById(long id) {
        Query query = entityManager.createQuery("DELETE FROM Book b WHERE b.id = :id");
        query.setParameter("id", id);
        return query.executeUpdate();
    }
}
