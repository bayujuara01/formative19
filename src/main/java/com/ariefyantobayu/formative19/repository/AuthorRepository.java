package com.ariefyantobayu.formative19.repository;

import com.ariefyantobayu.formative19.entity.Author;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;


@Repository
@Transactional
public class AuthorRepository {

    @PersistenceContext
    EntityManager entityManager;

    public List<Author> findAll() {
        TypedQuery<Author> query = entityManager.createQuery("SELECT a FROM Author a", Author.class);
        return query.getResultList();
    }

    public Author findById(long id) {
        Author author = null;
        TypedQuery<Author> query = entityManager.createQuery("SELECT a FROM Author a WHERE a.id=:id", Author.class);
        query.setParameter("id", id);

        try {
            author = query.getSingleResult();
        } catch (Exception e) {
            author = null;
        }
        return author;
    }

    public int insert(Author author) {
        Query query = entityManager.createNativeQuery("INSERT INTO author (name, email) VALUES (:name, :email)");
        query.setParameter("name", author.getName());
        query.setParameter("email", author.getEmail());
        return query.executeUpdate();
    }

    public int deleteById(long id) {
        Query query = entityManager.createQuery("DELETE FROM Author a WHERE a.id = :id");
        query.setParameter("id", id);
        return query.executeUpdate();
    }
}
