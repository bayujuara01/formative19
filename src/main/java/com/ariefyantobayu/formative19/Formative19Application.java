package com.ariefyantobayu.formative19;

import com.ariefyantobayu.formative19.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative19Application implements CommandLineRunner {

    @Autowired
    BaseController baseController;

    public static void main(String[] args) {
        SpringApplication.run(Formative19Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        baseController.run();
    }
}
