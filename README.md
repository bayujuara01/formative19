# formative19

## Nexsoft Fun Coding Bootcamp Formative 19

## Author
### Bayu Seno Ariefyanto

### Simple BookStore Application Using Spring Boot & JPA With Query & Relationship Between Entity

### 1. Sample Running Program
![Sample Running Program](sample-run.png "Running Program")

### 2. Running Program
```
====MAIN MENU====
1. Sub Menu Books
2. Sub Menu Author
Choice : 1
1. View All Book
2. View Book By Id
3. View Book By Author Id
4. Add Author [Not Implemented]
5. Delete Author [Not Implemented]
0. Back
Pilih : 1
=========ALL BOOK DATA=========
ID : 1
Title : Pemrograman PHP
Published : 2020
City : Purworejo
Author : Bayu Seno A
===============================
ID : 2
Title : Welcome Friends
Published : 2021
City : Purworejo
Author : Bayu Seno A
===============================
ID : 3
Title : Spring JPA For Beginner
Published : 2021
City : Yogyakata
Author : Aurelia
===============================

===============================
1. View All Book
2. View Book By Id
3. View Book By Author Id
4. Add Author [Not Implemented]
5. Delete Author [Not Implemented]
0. Back
Pilih : 2
Insert Book ID : 1
==========DETAIL BOOK==========
ID : 1
Title : Pemrograman PHP
Published : 2020
City : Purworejo
Author : Bayu Seno A
Contact : ariefyantobayu@gmail.com

===============================
1. View All Book
2. View Book By Id
3. View Book By Author Id
4. Add Author [Not Implemented]
5. Delete Author [Not Implemented]
0. Back
Pilih : 3
Insert Author ID : 2
=========ALL BOOK DATA=========
ID : 3
Title : Spring JPA For Beginner
Published : 2021
City : Yogyakata
Author : Aurelia
===============================

===============================
1. View All Book
2. View Book By Id
3. View Book By Author Id
4. Add Author [Not Implemented]
5. Delete Author [Not Implemented]
0. Back
Pilih : 0
====MAIN MENU====
1. Sub Menu Books
2. Sub Menu Author
Choice : 2
1. View All Author
2. View Author By Id
3. Add Author [Not Implemented]
4. Delete Author [Not Implemented]
0. Back
Pilih : 1
========ALL AUTHOR DATA========
ID : 1
Name : Bayu Seno A
Email : ariefyantobayu@gmail.com
===============================
ID : 2
Name : Aurelia
Email : aurelia.test@gmail.com
===============================

===============================
1. View All Author
2. View Author By Id
3. Add Author [Not Implemented]
4. Delete Author [Not Implemented]
0. Back
Pilih : 2
Insert Author ID : 1
==========DATA AUTHOR==========
ID : 1
Name : Bayu Seno A
Email : ariefyantobayu@gmail.com
===============================
1. View All Author
2. View Author By Id
3. Add Author [Not Implemented]
4. Delete Author [Not Implemented]
0. Back
Pilih : 0
====MAIN MENU====
1. Sub Menu Books
2. Sub Menu Author
Choice : 
```